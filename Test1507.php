<?php
try{
    $pdo = new PDO('mysql:host=localhost; dbname=test1507','root','');
    $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    $pdo->exec('SET NAMES "cp1251"');
}
catch (Exception $e){
    echo "ошибка при подключении Базы данных!". $e->getMessage();
    exit();
}

//---- Масив 'delete'----->

$sql=$pdo->query("SELECT ident FROM data ");
$result = $sql->fetchAll(PDO::FETCH_ASSOC);

/* Масив идентификаторов в БД */

$identDBArray =[];
foreach ($result as $value) {

    $identDBArray[]=$value['ident'];
}
$delete = array('delete'=>array_diff($_GET['ident'], $identDBArray));

/*Результат DELETE*/

echo '<pre>';
print_r($delete);

//---- Масив  'update'----->


/*Масив версий*/

$versionArr = [];
foreach ($_GET['version'] as $val){
    $versionArr[]=$val;
}
/*Масив идентификаторов в запросе*/

$identArr = [];
foreach ($_GET['ident'] as $v){
    $identArr[]=$v;
}
/*Масив Индентификатор=>версия*/
$combineArray = array_combine($identArr, $versionArr);

$update = [];
foreach ($combineArray as $id => $ver) {
    $sql2 = $pdo->query("SELECT ident, value, version FROM data WHERE ident='$id' AND version>'$ver'");
    $update[] = $sql2->fetch(PDO::FETCH_ASSOC);
}

if (!empty($update)) {
    foreach ($update as $val) {
        $arr[] = $val;
    }
} else {
    echo '<br>Большых версий в БД не найдены!';
}

/*Результат UPDATE*/

echo '<pre>';
print_r($up = array('update'=>$arr));


//---- Масив  'new'-------->

$newArrIdent = array_diff($identDBArray,$identArr);

if(!empty($newArrIdent)){
    $newArr =[];
    foreach ($newArrIdent as $val){
        $sql3=$pdo->query("SELECT ident, value, version FROM data WHERE ident='$val'");
        $newArr[] = $sql3->fetch(PDO::FETCH_ASSOC);
    }
} else {
    echo '<br>Отличных от БД Идентификаторы не найдены!';
}

/* Результат NEW */

echo '<pre>';
print_r($new = array('new'=>$newArr));